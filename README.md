# README: proj1-pageserver

**Author**: Lindsey Uribe

**Contact**: luribe@uoregon.edu

## A word game.

### What is this repository for?

This project is in the form of a game which utilizes Javascript with JQuery hooks
embedded in HTML to dynamically serve up page elements as opposed to entire-page 
refreshes to handle client-side form submissions. The objective of the game is to find 
a certain number of words from a list of jumbled letters. Five features are present in
this manifestation of the game which exemplify AJAX request-handling: 
 - Matching words are automatically found from the list of word-options along with key-up events
 - Correctly matched words are displayed below the entry form
 - Previously guessed words will be displayed as a 'flash' message if re-guessed
 - A countdown of remaining guesses needed to win the game is decremented dynamically

Additionally, 
 - Dynamic highlighting reveals potential words that may match with current text being entered in the form
 - Correctly matched words are "removed" (greyed out) from the list of potential options at the top of the page
 - Illegitimate letters are auto-removed from the form, and an error message is flashed, notifying users of bad letter guesses. 
