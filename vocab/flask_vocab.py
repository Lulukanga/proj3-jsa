"""
Flask web site with vocabulary matching game
(identify vocabulary words that can be made 
from a scrambled string)
"""

import flask
import logging

# Our own modules
from letterbag import LetterBag
from vocab import Vocab
from jumble import jumbled
import config

###
# Globals
###
app = flask.Flask(__name__)

CONFIG = config.configuration()
app.config['JSONIFY_PRETTYPRINT_REGULAR'] = False  # TODO? HAd to add this to make it work on my side

app.secret_key = CONFIG.SECRET_KEY  # Should allow using session variables

#
# One shared 'Vocab' object, read-only after initialization,
# shared by all threads and instances.  Otherwise we would have to
# store it in the browser and transmit it on each request/response cycle,
# or else read it from the file on each request/responce cycle,
# neither of which would be suitable for responding keystroke by keystroke.

WORDS = Vocab(CONFIG.VOCAB)

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    """The main page of the application"""
    flask.g.vocab = WORDS.as_list()
    flask.session["target_count"] = min(
        len(flask.g.vocab), CONFIG.SUCCESS_AT_COUNT)
    flask.session["jumble"] = jumbled(
        flask.g.vocab, flask.session["target_count"])
    flask.session["matches"] = []
    app.logger.debug("Session variables have been set")
    assert flask.session["matches"] == []
    assert flask.session["target_count"] > 0
    app.logger.debug("At least one seems to be set correctly")
    return flask.render_template('vocab.html')


@app.route("/keep_going")
def keep_going():
    """
    After initial use of index, we keep the same scrambled
    word and try to get more matches
    """
    flask.g.vocab = WORDS.as_list()
    return flask.render_template('vocab.html')


@app.route("/success")
def success():
    return flask.render_template('success.html')

#######################
# Form handler has been changed to a JSON request handler
#######################


# Setting up a server-side event for key-up
@app.route("/_check")
def check():
    """ This is where we check three things:
    1.) Is the text entered in letterbag? If not, errormsg
    2.) Is the text a word from our list of words?
    3.) Have we met minimum count of words needed?
    Note: flask.session is session-specific data."""
    text = flask.request.args.get("text", type=str)
    jumbled_letters = flask.session["jumble"]
    matched_words = flask.session.get("matches", [])

    is_in_matches = text in matched_words
    in_jumble = LetterBag(jumbled_letters).contains(text)
    is_in_wordlist = WORDS.has(text)
    target_count = flask.session["target_count"]

    if is_in_wordlist and in_jumble and not is_in_matches:
        matched_words.append(text)
        flask.session["matches"] = matched_words
        flask.session["target_count"] -= 1
        target_count -= 1

    partial_list = []
    for word in WORDS.as_list():
        if word.startswith(text) and in_jumble:
            partial_list.append(word)

    length = len(matched_words) >= CONFIG.SUCCESS_AT_COUNT
    rslt = flask.session["matches"]
    return flask.jsonify(result=rslt,
                         completed=length,
                         is_match=is_in_wordlist,
                         been_guessed=is_in_matches,
                         ok_letter=in_jumble,
                         target_ct=target_count,
                         partial_list=partial_list,
                         match_list=matched_words)


###############
# AJAX request handlers
#   These return JSON, rather than rendering pages.
###############

@app.route("/_example")
def example():
    """
    Example ajax request handler
    """
    app.logger.debug("Got a JSON request")
    rslt = {"key": "value"}
    return flask.jsonify(result=rslt)


#################
# Functions used within the templates
#################

@app.template_filter('filt')
def format_filt(something):
    """
    Example of a filter that can be used within
    the Jinja2 code
    """
    return "Not what you asked for"

###################
#   Error handlers
###################


@app.errorhandler(404)
def error_404(e):
    app.logger.warning("++ 404 error: {}".format(e))
    return flask.render_template('404.html'), 404


@app.errorhandler(500)
def error_500(e):
    app.logger.warning("++ 500 error: {}".format(e))
    assert not True  # I want to invoke the debugger
    return flask.render_template('500.html'), 500


@app.errorhandler(403)
def error_403(e):
    app.logger.warning("++ 403 error: {}".format(e))
    return flask.render_template('403.html'), 403


####

if __name__ == "__main__":
    if CONFIG.DEBUG:
        app.debug = True
        app.logger.setLevel(logging.DEBUG)
        app.logger.info(
            "Opening for global access on port {}".format(CONFIG.PORT))
        app.run(port=CONFIG.PORT, host="0.0.0.0")
